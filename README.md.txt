README multXsuby

1) Function

The function of the application is to find the sotrtest path from a strating number to a ending number(both user input),
this by multiplying by a number (user input) and sutracting by a number (user input)

2) Setup/How to use

When the program is launched: fill in the boxes with the appropriate values indicated by the label next to the text-box (ONLY NUMBERS).
Then click the button and if all goes well the calculations needed to complete the path will be displayed on the screen.
If a non valid number is entered the program will show an error message, remove the entered content and fill in the boxes correctly and reclick the button.

4)Project health

Project has given a correct result with the example given below

5) Testing

The best way to test is to folow along in the console and try different things, I tried to make a test class for the class that 
houses the tree but due to incapsolation of my node class i couldn't make nodes myself to test methods on. 
I guess that's partially a good thing

6)Options for help from external people



7)License information
At this point the software can be used by anyone as long as the autor Ruben Heintjens is mentioned in the used/ copied code

8) How does it work

	Example: going from 8 -->11 multiplier=2, subtractor=3 (every left child represents multiplication, the right child represents minus)

	
	when the program is launched correctly the Walktree method calls for a goDown.

	since the cursor == root && size ==1 (cursorvalue =8), add() will be called along with a new goDown() (added values are:
		16 and 6).

	now the cursor moves down (cursorvalue = 16) and a new goDown is called.

	since the cursor does not have children add() is called (added values are: 32 and 13) and also goRight() is called (cursorvalue =5)

	now a new goDown is called. but the cursor does not have children --> add()  (added values: 10 and 2) and goRigth()

	There is no more room to goRight --> goUp() (cursor = root, cursorvalue =8)

	here the procedure restarts with a goDown from the root 

	This continues untill a value == goal or untill a operation count limit is reached.
		(limit still to be implemented)

						                                                                8

					                         16	                                      CENTER		                             5

				     32                                                  13                   CENTER                    10                                     2

		          64                        29                      26                     10         CENTER           20                   7                4                   -1

		   128           61           58          26          52         23         20           7    CENTER     40         17         14       4        8       1          -2       -4

		256   125    122    58    116    55    52    23   104    49   46    20   40    17    14    4  CENTER  80    37   34   14   28    11  8    1  16    5  2    -2   -4    -5  -8    -7

	The shortest route is always the first correct solution since it will be at the highest possible palce in the tree.

9)Endresult

The program prints a string of textcontianing the calculations needed. the number of calculations needed is the height of the tree minus one
the size of the tree-1 represents the true amount of calculations since every adition of a node is a calculation of a new value (exept root) this value is printed to the console should
this be of your interest

10) Future improvements to be done:
	have more choices with the amount and kind of calculations (division = int-> double or rounding the numbers)
	a less busy and more easy to follow console to follow the program
	a way to detect when the solution is not possible to find:
		1/ Check at the moment of input 
		2/ Find paterns in the tree while running that the calculations are not improving
			example: when the same number returns in one path along the tree


Link to tips I used about readme.md
https://changelog.com/posts/a-beginners-guide-to-creating-a-readme