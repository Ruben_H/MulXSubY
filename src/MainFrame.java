import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JTextField;
/*
 * This class contains the rest of the program and sets a BorderLayout to add items to
 */
public class MainFrame extends JFrame {
	
	private ButtonPanel buttonPanel;
	private JTextArea field;
	private MathTree tree;
	/*
	 * constructor for the MainFrame
	 */
	public MainFrame (String title) {
		super (title);
		
		//set layout
		setLayout(new BorderLayout());
		//create components
		
		buttonPanel = new ButtonPanel();
		field = new JTextArea();
		
		//add components to pane
		Container c = getContentPane();
		
		c.add(buttonPanel, BorderLayout.WEST);
		c.add(field, BorderLayout.EAST);
		
		//actions
		
		buttonPanel.getCheckButton().addActionListener(new ActionListener() {

			@Override
			/*
			 * (non-Javadoc)
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 * when the button is pressed and valid numbers are given the calculation begins
			 */
			public void actionPerformed(ActionEvent arg0) {
				int multiplier = getInt( buttonPanel.getMultiplier());
				int subtractor = getInt( buttonPanel.getSubtractor());
				int start = getInt( buttonPanel.getStart());
				int goal = getInt( buttonPanel.getGoal());
				if (multiplier != 99999999 && subtractor != 99999999 && start != 99999999 && goal != 99999999) {
					action(start, multiplier, subtractor, goal);  
				}
				
				
			}
		
		
		});
		

	}
	/*
	 * method to check if a valid integer is inserted into the given JTextField
	 * @param JTextField text
	 */
	public int getInt(JTextField text) {
		
		try {
			buttonPanel.getWarning().setText("");
			return Integer.parseInt(text.getText());
		} catch (NumberFormatException e) {
			//Will Throw exception!
			//do something! anything to handle the exception.
			buttonPanel.getWarning().setText("Enter valid numbers and try again ;) ");
			return 99999999;
		}
		
	}
	
	/*
	 * This method is called by the MainFrame to start the calculations
	 */
	public void action(int value, int timesFactor, int minusFactor, int goal) {
		this.tree = new MathTree(value, timesFactor, minusFactor, goal);
		field.setText(tree.walkTree());
		
	}
	
	
}
