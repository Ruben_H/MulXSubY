import java.util.ArrayList;
/*
 * class to make the tree and all things concerning the tree (operations, walking, adding, ...)
 * contains a subclass treeNode
 */
public class MathTree<Node> {
	private int size, maxChildren, timesFactor, minusFactor, goal;
	private Node root;
	private ArrayList path;
	private int times = 0;
	private boolean pathFound;
	private String resultingRoute;
	/*
	 * no argument constructor for a tree
	 */
	public MathTree() {
		this.size = 0;
		this.root = null;
		this.maxChildren = 1;
		
	}
	/*
	 * constructor for a tree
	 * @Param int value (startvalue to start calculations from, will be stored in the root)
	 * @Param int timesFactor (the factor used by the multiplication operation)
	 * @Param int minusFactor (the factor used by the minus operation)
	 * @Param int goal (end result of the calculations (user defined))
	 * 
	 */
	public MathTree(int value, int timesFactor, int minusFactor, int goal) {
		this.root = new Node(value, null, true);
		this.size = 1;
		this.maxChildren = 1;
		this.timesFactor = timesFactor;
		this.minusFactor = minusFactor;
		this.goal = goal;
		this.pathFound = false;
		
	}
	/*
	 * method to add children to a node and also setting the correct value in their value parameter
	 * @Param Node parent
	 */
	public void add(Node parent) {
		if (parent != null) {
			int value1 = simpleCalculate (parent.value,0);
			int value2 = simpleCalculate (parent.value,1);
			System.out.println("addMethod");
			System.out.println("value1= " + value1);
			System.out.println("value2= "+ value2);
			Node newNode = new Node(value1, parent, false);
			newNode.setHasChildren(false);
			Node newNode2 = new Node(value2, parent, false);
			newNode2.setHasChildren(false);
			parent.children.clear();
			parent.children.add(0,newNode);
			parent.children.add(1,newNode2);
			parent.setHasChildren(true);
			
			this.size+=2;
		}
	}
	
	/*
	 * method to start walking the tree and looking for a path
	 * 
	 * this method calls goDown(ArrayList<int> path,Node cursor) and this method will call other methods (goRight(ArrayList<int> path,Node cursor), goUp(ArrayList<int> path,Node cursor)) to walk the tree
	 */
	public String walkTree() {
		Node cursor = this.root;
		path = new ArrayList<Integer>();
		for (int i =0; i<10; i++) {
			path.add(i,0);
		}
		while(!pathFound ) {
			//&& limit>calculations
			goDown(path,cursor);
		}
		//return printPath(path);
		return resultingRoute;
		
	}
	
	/*
	 * method called when movement up the tree is needed
	 * 
	 * as long as the "upper" node (parent) of the cursor != root goUp is followed by a goRight
	 * else a goDown is called 
	 */
	public void goUp(ArrayList path, Node cursor) {
		
		cursor = cursor.parent;
		if (!cursor.isRoot) {
			goRight(path,cursor);
		}
		else {
			goDown(resetPath(path, 0),cursor);
			System.out.println("I passed rood bc of goUp");
		}
	}
	/*
	 * this method is called when a movement to the right is needed
	 * 
	 */
	public void goRight(ArrayList path, Node cursor) {
		System.out.println("value cursor before onright = "+cursor.getValue());
		path = resetPath( path, cursor.getHeight());
		if ((int) path.get(cursor.parent.getHeight()) < maxChildren) {
			System.out.println("goRight is possible");
			path.set(cursor.parent.getHeight(), (int) path.get(cursor.parent.getHeight())+1);
			cursor = cursor.parent.children.get(((int) path.get(cursor.parent.getHeight())));
			if (cursor.value == goal) {
				pathFound = true;
				resultingRoute = printPath(path,cursor);
			}
			else {
				goDown(path,cursor);
				
			}
			
		}
		else {
			goUp (path,cursor);
		}
	}
	/*
	 * this method is called when a movement down the tree is needed
	 * this method also extends the tree by adding children to nodes without children to make sure the tree has grown when the cursor passes by the next time
	 */
	public void goDown(ArrayList path, Node cursor) {
		int location;
		System.out.println("value cursor before down = "+cursor.getValue());
		System.out.println("size of the tree while in goDown = "+this.size);
		if (cursor.isRoot && this.size ==1) {
			add(cursor);
			goDown(path,cursor);
		}
		else {
			//System.out.println("hasChild = "+cursor.getHasChildren());
			if (cursor.getHasChildren() == true) {
				System.out.println("path when children= "+path.toString());
				location =  (int) path.get(cursor.getHeight());
				
				if (location <= maxChildren) {
					System.out.println("children = [ "+cursor.children.get(0).getValue() +" "+cursor.children.get(1).getValue() +" ] ");
					System.out.println("this is location = "+location);
					cursor = cursor.children.get(location);
					if (cursor.getValue() != goal) {
						goDown(path, cursor);
					}
					else {
						pathFound = true;;
						resultingRoute = printPath(path,cursor);
					}
				}
				else {
					System.out.println("Invalid child index found");
				}
			}
			else {
				add(cursor);
				path.add(path.size()-1,0);
				goRight(path, cursor);
			}
		}
	}
	/*
	 * this method is called when adding children to the tree to calculate their values based on being left or right child
	 */
	public int simpleCalculate(int value, int choice) {
		if (choice == times) {
			return value *= timesFactor;
		}
		else {
			return value -= minusFactor;
		}
	}
	
	/*
	 * this method is called to print the path to be able to show the calculations needed to get from start to goal
	 */
	public String printPath(ArrayList path, Node cursor) {
		StringBuffer str = new StringBuffer(root.value+" ");
		for (int i=0;i<cursor.getHeight();i++) {
			if ((int) path.get(i) == times) {
				str.append("(* "+ timesFactor+" ) ");
			}
			else {
				str.append("(- "+ minusFactor+" ) ");
			}
		}
		str.append("= "+ goal);
		return str.toString();
	}
	
	/*
	 * this method resets the path when a goRight is executed (this method is only called from goRight) to make sure that no nodes would be skipped
	 * 
	 */
	public ArrayList resetPath(ArrayList path, int startIndex) {
		ArrayList temp = new ArrayList();
		for (int i=0; i<path.size();i++) {
			temp.add(0);
		}
		System.out.println("this is temp before reset: "+temp.toString());
		for (int i=0; i< startIndex;i++) {
			System.out.println("this is path at reset: "+path.toString());
			temp.set(i, path.get(i));
		}
		return temp;
	}
	
	
	/**
	 * This is a node containing a value 
	 * 
	 *
	 * @param <E> type parameter
	 * @author Piet Cordemans & Ruben Heintjens
	 */
	private class Node {
		private int value;
		private Node parent;
		private ArrayList<Node> children;
		private boolean isRoot, hasChildren;
		private int height;
		
		/***
		 * Creates a node with a single element, next referring to null
		 * 
		 * @param value
		 */
		public Node(int value) {
			this(value, null, false);
		}
		
		/**
		 * Creates a node with a single element, next referring to the next node
		 * @param value
		 * @param next
		 */
		public Node(int value, Node parent, boolean isRoot) {
			this.hasChildren = false;
			this.children = new ArrayList(2);
			children.add(0, null);
			children.add(1, null);
			this.value = value;
			this.parent = parent;
			this.isRoot = isRoot;
			if (isRoot) {this.height = 0;}
			else {this.height = parent.height+1;}
		}
		
		/**
		 * Getter for the element
		 *  
		 * @return element
		 */
		public int getValue() {
			return value;
		}
		
		/**
		 * Getter for the children array
		 * 
		 * @return children array
		 */
		public ArrayList getChildren(){
			return children;
		}
		
		public int getHeight() {
			return height;
		}
		
		public boolean getHasChildren() {
			return this.hasChildren;
		}
		
		public void setElement(int value) {
			this.value = value;
		}
		
		public void setHasChildren(boolean bool) {
			this.hasChildren = bool;
		}


	}

}
