import javax.swing.JFrame;
import javax.swing.SwingUtilities;
/*
 * This class starts the program, creates a MainFrame
 */
public class MulXSubY_Main_App {
	
	public static void main(String[] args) {
		
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				JFrame frame = new MainFrame("Math Tree");
				frame.setSize (1000,400);
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setVisible(true);
				
			}
		});
		
	}

}
